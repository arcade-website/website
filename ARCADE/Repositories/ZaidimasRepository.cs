﻿using System;
using System.Data;
using MySql.Data.MySqlClient;
using System.Collections.Generic;

using ARCADE.ViewModels;

namespace ARCADE.Repositories
{
    public class ZaidimasRepository
    {
        public static List<ZaidimasListVM> List()
        {
            var result = new List<ZaidimasListVM>();

            var query =
                $@"SELECT
                    zaidimai.paveikslelio_kelias
                FROM
                    zaidimai";

            var dt = Sql.Query(query);

            foreach( DataRow item in dt)
            {
                result.Add(new ZaidimasListVM
                {
                    Pavadinimas = Convert.ToString(item["pavadinimas"]),
                    Platforma = Convert.ToString(item["platforma"]),
                    Kaina = Convert.ToDouble(item["kaina"]),
                    Bukle = Convert.ToString(item["bukle"]),
                    Zanras = Convert.ToString(item["zanras"]),
                    Kurejai = Convert.ToString(item["kurejai"]),
                    Paveikslelio_kelias = Convert.ToString(item["paveikslelio_kelias"])
                });
            }

            return result;
        }

        public static ZaidimasListVM Find(int id)
        {
            var zaidimas = new ZaidimasListVM();

            var query = $@"SELECT * FROM zaidimai WHERE zaidimo_kodas=?id";

            var dt =
                Sql.Query(query, args => {
                    args.Add("?id", MySqlDbType.Int32).Value = id;
                });

            foreach (DataRow item in dt)
            {
                zaidimas.Pavadinimas = Convert.ToString(item["pavadinimas"]);
                zaidimas.Platforma = Convert.ToString(item["platforma"]);
                zaidimas.Kaina = Convert.ToDouble(item["kaina"]);
                zaidimas.Bukle = Convert.ToString(item["bukle"]);
                zaidimas.Zanras = Convert.ToString(item["zanras"]);
                zaidimas.Kurejai = Convert.ToString(item["kurejai"]);
                zaidimas.Paveikslelio_kelias = Convert.ToString(item["paveikslelio_kelias"]);
            }

            return zaidimas;
        }

        public static void Delete(int id)
        {
            var query = $@"DELETE FROM zaidimai where zaidimo_kodas=?id";
            Sql.Delete(query, args => {
                args.Add("?id", MySqlDbType.Int32).Value = id;
            });
        }
    }
}
