﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;


using ARCADE.ViewModels;
using ARCADE.Repositories;

namespace ARCADE.Controllers
{
    public class ZaidimasController : Controller
    {
        public ActionResult Index()
        {
            return View(ZaidimasRepository.List());
        }

        public ActionResult Delete(int id)
        {
            var zaidimas = ZaidimasRepository.Find(id);
            return View(zaidimas);
        }

		[HttpPost]
		public ActionResult DeleteConfirm(int id)
		{
			try
			{
				ZaidimasRepository.Delete(id);
				return RedirectToAction("Index");
			}
			catch (MySql.Data.MySqlClient.MySqlException)
			{
				ViewData["deletionNotPermitted"] = true;
				var zaidimas = ZaidimasRepository.Find(id);
				return View("Delete", zaidimas);
			}
		}
	}
}
