﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ARCADE.Pages
{
    public class Zaidimas
    {
        public string ID { get; set; }
        public string Pavadinimas { get; set; }
        public string Platforma { get; set; }
        public double Kaina { get; set; }
        public string Zanras { get; set; }
        public string Bukle { get; set; }

        public string Nuotrauka { get; set; }
    }
}
