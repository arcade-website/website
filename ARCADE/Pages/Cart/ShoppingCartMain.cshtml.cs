using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.IO;

/// <summary>
/// REIKIA PADARYTI, KAD B�T� GRA�INAMI �D�TI � KREP�EL� �AIDIMAI
/// </summary>
namespace ARCADE.Pages.Shopping_Cart
{
    public class ShoppingCartMainModel : PageModel
    {
            public Zaidimas zaidimas { get; set; } = new Zaidimas();
            public List<Zaidimas> zaidimai { get; set; }
            public void OnGet()
            {
                zaidimai = GetZaidimai();
            }

            List<Zaidimas> GetZaidimai()
            {
                List<Zaidimas> zaidimai = new List<Zaidimas>();
                using (var reader = new StreamReader(@"../ARCADE/wwwroot/data/Zaidimai.csv"))
                {

                    string line = "";
                    while ((line = reader.ReadLine()) != null)
                    {
                        Zaidimas zaidimas = new Zaidimas();
                        string[] values = line.Split(',');
                        zaidimas.ID = values[0];
                        zaidimas.Pavadinimas = values[1];
                        zaidimas.Platforma = values[2];
                        zaidimas.Kaina = double.Parse(values[3]);
                        zaidimas.Zanras = values[4];
                        zaidimas.Bukle = values[5];
                        zaidimas.Nuotrauka = string.Format("~/images/Zaidimai/" + values[0] + ".jpg");

                        //if (zaidimas.Zanras == "Lenktyni�")
                        //    zaidimai.Add(zaidimas);
                    }
                }

                return zaidimai;
            }
        }
}
