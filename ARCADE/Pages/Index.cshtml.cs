﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ARCADE.Pages
{
    public class IndexModel : PageModel
    {
        public Zaidimas zaidimas { get; set; } = new Zaidimas();
        public List<Zaidimas> zaidimai { get; set; }
        public void OnGet()
        {
            zaidimai = GetZaidimai();
        }

        List<Zaidimas> GetZaidimai()
        {
            int count = 0;
            List<Zaidimas> zaidimai = new List<Zaidimas>();
            using (var reader = new StreamReader(@"../ARCADE/wwwroot/data/Zaidimai.csv"))
            {

                string line = "";
                while ((line = reader.ReadLine()) != null && count < 11)
                {
                    Zaidimas zaidimas = new Zaidimas();
                    string[] values = line.Split(',');
                    zaidimas.ID = values[0];
                    zaidimas.Pavadinimas = values[1];
                    zaidimas.Platforma = values[2];
                    zaidimas.Kaina = double.Parse(values[3]);
                    zaidimas.Zanras = values[4];
                    zaidimas.Bukle = values[5];
                    zaidimas.Nuotrauka = string.Format("~/images/Zaidimai/" + values[0] + ".jpg");

                    zaidimai.Add(zaidimas);
                    count++;
                }
            }

            return zaidimai;
        }
    }
}
