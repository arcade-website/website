﻿using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ARCADE.ViewModels
{
    public class ZaidimasListVM
    {
        [DisplayName("Žaidimo kodas")]
        public int Zaidimo_kodas { get; set; }

        [DisplayName("Pavadinimas")]
        public string Pavadinimas { get; set; }

        [DisplayName("Platforma")]
        public string Platforma { get; set; }

        [DisplayName("Kaina")]
        public double Kaina { get; set; }

        [DisplayName("Būklė")]
        public string Bukle { get; set; }

        [DisplayName("Žanras")]
        public string Zanras { get; set; }

        [DisplayName("Kūrėjai")]
        public string Kurejai { get; set; }

        public string Paveikslelio_kelias { get; set; }
    }
}
