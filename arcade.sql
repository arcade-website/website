-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2022 at 11:12 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arcade`
--

-- --------------------------------------------------------

--
-- Table structure for table `asmuo`
--

CREATE TABLE `asmuo` (
  `kodas` varchar(255) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL,
  `el_pastas` varchar(255) NOT NULL,
  `slaptazodis` varchar(255) NOT NULL,
  `telefono_numeris` varchar(255) NOT NULL,
  `fk_MIESTAS` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `asmuo`
--

INSERT INTO `asmuo` (`kodas`, `vardas`, `pavarde`, `el_pastas`, `slaptazodis`, `telefono_numeris`, `fk_MIESTAS`) VALUES
('1000', 'Simona', 'Petraitytė', 'simona.petraityte@gmail.com', '', '865412357', 'D105'),
('1001', 'Petras', 'Kazlauskas', 'petras.kazlauskas@gmail.com', '', '864597123', 'K100'),
('1002', 'Veronika', 'Kazlauskaitė', 'veronika.kazlauskaite@gmail.com', '', '865987458', 'K104'),
('2003', 'Birutė', 'Babiliūtė', 'birute.babiliute@gmail.com', '', '864785962', 'T103'),
('2004', 'Justas', 'Kalnaitis', 'justas.kalnaitis@gmail.com', '', '864357415', 'V101'),
('2005', 'Iveta', 'Rimkutė', 'iveta.rimkute@gmail.com', '', '869542165', 'Š102');

-- --------------------------------------------------------

--
-- Table structure for table `atsiliepimas`
--

CREATE TABLE `atsiliepimas` (
  `autoriaus_kodas` varchar(255) NOT NULL,
  `autorius` varchar(255) NOT NULL,
  `ivertinmas` int(11) NOT NULL,
  `komentaras` varchar(255) DEFAULT NULL,
  `data` date NOT NULL,
  `fk_pardavejas` varchar(255) DEFAULT NULL,
  `fk_ZAIDIMAS` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `autorius`
--

CREATE TABLE `autorius` (
  `kodas` varchar(255) NOT NULL,
  `vardas` varchar(255) NOT NULL,
  `pavarde` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `busena`
--

CREATE TABLE `busena` (
  `id_busena` int(11) NOT NULL,
  `name` char(9) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `busena`
--

INSERT INTO `busena` (`id_busena`, `name`) VALUES
(1, 'pateiktas'),
(2, 'gautas'),
(3, 'vykdomas'),
(4, 'ivykdytas');

-- --------------------------------------------------------

--
-- Table structure for table `miestas`
--

CREATE TABLE `miestas` (
  `pavadinimas` varchar(255) NOT NULL,
  `kodas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `miestas`
--

INSERT INTO `miestas` (`pavadinimas`, `kodas`) VALUES
('Druskininkai', 'D105'),
('Kaunas', 'K100'),
('Klaipėda', 'K104'),
('Šiauliai', 'Š102'),
('Tauragė', 'T103'),
('Vilnius', 'V101');

-- --------------------------------------------------------

--
-- Table structure for table `nuolaida`
--

CREATE TABLE `nuolaida` (
  `procentai` double NOT NULL,
  `nuo` date NOT NULL,
  `iki` date NOT NULL,
  `fk_ZAIDIMAS` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `platforma`
--

CREATE TABLE `platforma` (
  `id_konsole` int(11) NOT NULL,
  `name` char(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platforma`
--

INSERT INTO `platforma` (`id_konsole`, `name`) VALUES
(1, 'asmeninis_komp'),
(2, 'playstation_2'),
(3, 'playstation_3'),
(4, 'playstation_vita'),
(5, 'playstation_4'),
(6, 'playstation_5'),
(7, 'xbox_360'),
(8, 'xbox_one'),
(9, 'nintendo_wii'),
(10, 'nintendo_3ds'),
(11, 'nintendo_switch');

-- --------------------------------------------------------

--
-- Table structure for table `sukuria`
--

CREATE TABLE `sukuria` (
  `fk_autorius` varchar(255) NOT NULL,
  `fk_zaidimas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `uzsakymas`
--

CREATE TABLE `uzsakymas` (
  `nr` int(11) NOT NULL,
  `data` date NOT NULL,
  `suma` double NOT NULL,
  `busena` int(11) NOT NULL,
  `fk_uzsakovas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `uzsakymas`
--

INSERT INTO `uzsakymas` (`nr`, `data`, `suma`, `busena`, `fk_uzsakovas`) VALUES
(2, '2022-03-05', 30, 1, '1001'),
(1, '2022-03-10', 25, 1, '1000'),
(3, '2022-03-12', 20, 1, '1002');

-- --------------------------------------------------------

--
-- Table structure for table `zaidimas`
--

CREATE TABLE `zaidimas` (
  `pavadinimas` varchar(255) NOT NULL,
  `kaina` double NOT NULL,
  `kureju_kompanija` varchar(255) DEFAULT NULL,
  `bukle` varchar(255) NOT NULL,
  `zaidimo_kodas` int(11) NOT NULL,
  `zanras` int(11) NOT NULL,
  `platforma` int(11) NOT NULL,
  `paveikslelio_kelias` varchar(255) NOT NULL,
  `fk_UZSAKYMAS` date DEFAULT NULL,
  `fk_UZSAKYMAS1` int(11) DEFAULT NULL,
  `fk_pardavejas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zaidimas`
--

INSERT INTO `zaidimas` (`pavadinimas`, `kaina`, `kureju_kompanija`, `bukle`, `zaidimo_kodas`, `zanras`, `platforma`, `paveikslelio_kelias`, `fk_UZSAKYMAS`, `fk_UZSAKYMAS1`, `fk_pardavejas`) VALUES
('Battlefield 5(Year 2 Edition)', 24.99, 'EA Digital Illusions/EA DICE', 'Naujas', 5000, 6, 8, '0', '2022-03-10', 1, '2003'),
('LEGO Worlds', 14.95, 'TT Games', 'Naujas', 5001, 1, 8, '0', '2022-03-05', 2, '2004');

-- --------------------------------------------------------

--
-- Table structure for table `zanras`
--

CREATE TABLE `zanras` (
  `id_zanras` int(11) NOT NULL,
  `name` char(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `zanras`
--

INSERT INTO `zanras` (`id_zanras`, `name`) VALUES
(1, 'nuotykiu'),
(2, 'veiksmo'),
(3, 'vaidmenu_rpg'),
(4, 'siaubo'),
(5, 'strateginis'),
(6, 'kovinis'),
(7, 'indie'),
(8, 'platforminis'),
(9, 'lenktyniu'),
(10, 'simuliacinis'),
(11, 'sporto');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `asmuo`
--
ALTER TABLE `asmuo`
  ADD PRIMARY KEY (`kodas`),
  ADD KEY `fk_MIESTAS` (`fk_MIESTAS`);

--
-- Indexes for table `atsiliepimas`
--
ALTER TABLE `atsiliepimas`
  ADD PRIMARY KEY (`autoriaus_kodas`),
  ADD KEY `nusako` (`fk_pardavejas`);

--
-- Indexes for table `autorius`
--
ALTER TABLE `autorius`
  ADD PRIMARY KEY (`kodas`);

--
-- Indexes for table `busena`
--
ALTER TABLE `busena`
  ADD PRIMARY KEY (`id_busena`);

--
-- Indexes for table `miestas`
--
ALTER TABLE `miestas`
  ADD PRIMARY KEY (`kodas`);

--
-- Indexes for table `nuolaida`
--
ALTER TABLE `nuolaida`
  ADD PRIMARY KEY (`fk_ZAIDIMAS`),
  ADD UNIQUE KEY `fk_ZAIDIMAS` (`fk_ZAIDIMAS`);

--
-- Indexes for table `platforma`
--
ALTER TABLE `platforma`
  ADD PRIMARY KEY (`id_konsole`);

--
-- Indexes for table `sukuria`
--
ALTER TABLE `sukuria`
  ADD PRIMARY KEY (`fk_autorius`,`fk_zaidimas`);

--
-- Indexes for table `uzsakymas`
--
ALTER TABLE `uzsakymas`
  ADD PRIMARY KEY (`data`,`nr`),
  ADD KEY `pateikia` (`fk_uzsakovas`),
  ADD KEY `busena` (`busena`);

--
-- Indexes for table `zaidimas`
--
ALTER TABLE `zaidimas`
  ADD PRIMARY KEY (`zaidimo_kodas`),
  ADD KEY `sudaro` (`fk_UZSAKYMAS`,`fk_UZSAKYMAS1`),
  ADD KEY `parduoda` (`fk_pardavejas`),
  ADD KEY `zanras` (`zanras`),
  ADD KEY `platforma` (`platforma`) USING BTREE;

--
-- Indexes for table `zanras`
--
ALTER TABLE `zanras`
  ADD PRIMARY KEY (`id_zanras`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `asmuo`
--
ALTER TABLE `asmuo`
  ADD CONSTRAINT `asmuo_ibfk_1` FOREIGN KEY (`fk_MIESTAS`) REFERENCES `miestas` (`kodas`);

--
-- Constraints for table `atsiliepimas`
--
ALTER TABLE `atsiliepimas`
  ADD CONSTRAINT `nusako` FOREIGN KEY (`fk_pardavejas`) REFERENCES `asmuo` (`kodas`);

--
-- Constraints for table `nuolaida`
--
ALTER TABLE `nuolaida`
  ADD CONSTRAINT `taikoma` FOREIGN KEY (`fk_ZAIDIMAS`) REFERENCES `zaidimas` (`zaidimo_kodas`);

--
-- Constraints for table `sukuria`
--
ALTER TABLE `sukuria`
  ADD CONSTRAINT `sukuria` FOREIGN KEY (`fk_autorius`) REFERENCES `autorius` (`kodas`);

--
-- Constraints for table `uzsakymas`
--
ALTER TABLE `uzsakymas`
  ADD CONSTRAINT `pateikia` FOREIGN KEY (`fk_uzsakovas`) REFERENCES `asmuo` (`kodas`),
  ADD CONSTRAINT `uzsakymas_ibfk_1` FOREIGN KEY (`busena`) REFERENCES `busena` (`id_busena`),
  ADD CONSTRAINT `uzsakymas_ibfk_2` FOREIGN KEY (`busena`) REFERENCES `busena` (`id_busena`);

--
-- Constraints for table `zaidimas`
--
ALTER TABLE `zaidimas`
  ADD CONSTRAINT `parduoda` FOREIGN KEY (`fk_pardavejas`) REFERENCES `asmuo` (`kodas`),
  ADD CONSTRAINT `sudaro` FOREIGN KEY (`fk_UZSAKYMAS`,`fk_UZSAKYMAS1`) REFERENCES `uzsakymas` (`data`, `nr`),
  ADD CONSTRAINT `zaidimas_ibfk_1` FOREIGN KEY (`zanras`) REFERENCES `zanras` (`id_zanras`),
  ADD CONSTRAINT `zaidimas_ibfk_2` FOREIGN KEY (`platforma`) REFERENCES `platforma` (`id_konsole`),
  ADD CONSTRAINT `zaidimas_ibfk_3` FOREIGN KEY (`platforma`) REFERENCES `platforma` (`id_konsole`),
  ADD CONSTRAINT `zaidimas_ibfk_4` FOREIGN KEY (`zanras`) REFERENCES `zanras` (`id_zanras`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
